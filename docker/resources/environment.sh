#!/usr/bin/env bash

# (c) 2018, Andres Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.

# per environments
egroups=(
   "itsupport:10100"
   "itdevelopment:10200"
   "itoperations:10300"
   "itarchitecture:10400"
   "itmanagers:10500"
)

echo "Adding groups.. "
groupcmd=$(which groupadd)
if [ $? -eq 0 ]; then
   for egroup in ${egroups[*]}; do
      igroup=(${egroup//:/ })
      gname=${igroup[0]}
      gid=${igroup[1]}
      ${groupcmd} --gid ${gid} ${gname}
      echo "[${gname}]"
   done
else
   echo "Ya valio"
   exit -1
fi

# users
edomain="silanes.host"
eusers=(
   "10100:/bin/bash:itsupport:Support_<supuser(at)${edomain}:usupport"
   "10200:/bin/bash:itdevelopment:Development_<devuser(at)${edomain}>:udeveloper"
   "10201:/bin/bash:itdevelopment:Ricardo_Avila_<ravila(at)${edomain}>:ricardoavila"
   "10300:/bin/bash:itoperations:Operations_<opeuser(at)${edomain}>:uoperator"
   "10400:/bin/bash:itarchitecture:Architecture_<arquser(at)${edomain}>:uarchitect"
   "10401:/bin/bash:itarchitecture:Andres_Aquino_<aaquino(at)${edomain}>:andresaquino"
)

echo "Adding users.. "
usercmd=$(which useradd)
if [ $? -eq 0 ]; then
   for euser in ${eusers[*]}; do
      iuser=(${euser//:/ })
      uid=${iuser[0]}
      ushell=${iuser[1]}
      ugroup=${iuser[2]}
      udescription=${iuser[3]}
      uaccount=${iuser[4]}
      ${usercmd} --create-home --uid ${uid} --shell ${ushell} --gid ${ugroup} --comment "${udescription}" ${uaccount}
      echo "[${uaccount}:${udescription}]"

      # and set passwds
      echo "${uaccount}:123qwe" | chpasswd > /dev/null 2>&1
   done
fi

# sudoers
cp -p /etc/sudoers /etc/sudoers.backup
sed -e '1,/%wheel/ s/.*%wheel.*/%itdevelopment\tALL=(ALL)\tNOPASSWD: ALL/g' /etc/sudoers > /etc/sudoers.prev
mv /etc/sudoers.prev /etc/sudoers
chmod 0440 /etc/sudoers

