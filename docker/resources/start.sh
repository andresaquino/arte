#!/usr/bin/env bash

# (c) 2018, Andres Aquino <inbox@andresaquino.sh>
# This file is licensed under the BSD License version 3 or later.
# See the LICENSE file.
# 
 
cd /opt/vimzheimer
bash vimzheimer.start
 
